package ch.hackzurich15.cartinder.cartinder;

/**
 * Created by Zsombor Kalotay on 03.10.2015.
 */
public class CTBrand {

    private int brandCount;
    private String brand;

    public  CTBrand(String brand){
        brandCount = 0;
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getBrandCount() {
        return brandCount;
    }

    public void increaseBrandCount() {
        brandCount += 1;
    }



}
