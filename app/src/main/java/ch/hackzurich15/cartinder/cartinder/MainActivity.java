package ch.hackzurich15.cartinder.cartinder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    public static final String USER_PREFS_NAME ="userData";
    private Button submitButton;
    private EditText mNameEditText, mEmailEditText, mBirthdayEditText;
    private AutoCompleteTextView mHomeAddress, mWorkAddress;
    private EditText mChildren;
    private Spinner mCarTypeSpinner;
    private CTPerson mPerson;
    private String name, carType, eMail, birthdayString, homeAddress, workAddress;
    private Date birhtday;
    private int job, numberOfChildren;
    private String[]  dataBrands, dataGearTypes, dataFuelTypes;
    private ArrayList<String> colorList, fuelList, gearList, brandList;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        colorList = new ArrayList<>();
        fuelList = new ArrayList<>();
        brandList = new ArrayList<>();
        gearList = new ArrayList<>();


        submitButton = (Button) findViewById(R.id.submitBt);
        mNameEditText = (EditText) findViewById((R.id.Name));
        mEmailEditText = (EditText) findViewById(R.id.mEmailAddress);
        mBirthdayEditText = (EditText) findViewById(R.id.birthday);
        mHomeAddress = (AutoCompleteTextView) findViewById(R.id.home_address);
        mHomeAddress.setAdapter(new AutoCompleteAdapter(this));
        mWorkAddress = (AutoCompleteTextView) findViewById(R.id.work_address);
        mWorkAddress.setAdapter(new AutoCompleteAdapter(this));
        mCarTypeSpinner = (Spinner) findViewById(R.id.carTypeSpinner);
        mChildren = (EditText) findViewById(R.id.children);









        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        final SharedPreferences.Editor userDataEditor = getSharedPreferences(USER_PREFS_NAME,0).edit();
        getData("colors");
        getData("fuelTypes");
        getData("gearTypes");
        getData("brands");



        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                name = mNameEditText.getText().toString();
                eMail = mEmailEditText.getText().toString();
                birthdayString = mBirthdayEditText.getText().toString();
                SimpleDateFormat dateFormat = new SimpleDateFormat("DD.MM.yyyy");
                try {
                    Date birthday = dateFormat.parse(birthdayString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                carType = String.valueOf(mCarTypeSpinner.getSelectedItemId());
                homeAddress = mHomeAddress.getText().toString();
                workAddress = mWorkAddress.getText().toString();
                numberOfChildren = Integer.parseInt(mChildren.getText().toString());
                String[] dataColors = new String[colorList.size()];
                dataColors = colorList.toArray(dataColors);
                dataGearTypes = gearList.toArray(new String[gearList.size()]);
                dataFuelTypes = fuelList.toArray(new String[fuelList.size()]);
                dataBrands = brandList.toArray(new String[brandList.size()]);
                Log.d("Array", dataColors[1]);

                mPerson = new CTPerson(name, eMail, null, carType, null, null, numberOfChildren, dataColors, dataBrands, dataGearTypes, dataFuelTypes);     //TODO Change Null
                Gson gson = new Gson();
                String jsonPerson = gson.toJson(mPerson);
                userDataEditor.putString("User", jsonPerson).commit();

                final Intent intent = new Intent(MainActivity.this, MatcherActivity.class);
                startActivity(intent);

            }
        });

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void getData(String dataType){
        DataAsyncTask mAsync;
        mAsync = new DataAsyncTask();
        switch (dataType){
            case "colors": mAsync.execute("http://api.hackzurich.amag.ch/hackzurich/color/exterior.json",dataType);
                break;
            case "fuelTypes": mAsync.execute("http://api.hackzurich.amag.ch/hackzurich/fueltype.json",dataType);
                break;
            case "gearTypes": mAsync.execute("http://api.hackzurich.amag.ch/hackzurich/geartype.json",dataType);
                break;
            case "brands":{ brandList.add("SKODA");
                            brandList.add("Audi");
                            brandList.add("VW");
                            brandList.add("SEAT");
                            brandList.add("VWNF");

                }
                break;
            default:
                break;
        }

    }

    public class DataAsyncTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(params[0]);
                HttpResponse response = client.execute(get);

                int status = response.getStatusLine().getStatusCode();
                if (status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    JSONObject jObj = new JSONObject(data);
                    JSONArray jArray =jObj.getJSONArray(params[1]);
                    for(int i=0; i<jArray.length(); i++){
                        JSONObject jRealObject = jArray.getJSONObject(i);
                        String id = jRealObject.getString("id");
                        switch (params[1]){
                            case "colors": colorList.add(id);
                                break;
                            case "fuelTypes": fuelList.add(id);
                                break;
                            case "gearTypes": gearList.add(id);
                                break;
                            default:
                                break;
                        }

                    }
                    Log.d("ARRAYLISTColor", String.valueOf(colorList));
                    Log.d("ARRAYLISTFuel", String.valueOf(fuelList));
                    Log.d("ARRAYLISTGear", String.valueOf(gearList));
                    Log.d("ARRAYLISTBrand", String.valueOf(brandList));


                    return true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(result == false){
                // show a msg that data was not parsed
            } else{

            }
        }
    }
}
