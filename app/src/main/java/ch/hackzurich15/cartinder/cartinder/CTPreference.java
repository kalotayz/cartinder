package ch.hackzurich15.cartinder.cartinder;

import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;

import ch.hackzurich15.cartinder.cartinder.database.DatabaseCommunicator;

/**
 * Created by Zsombor Kalotay on 03.10.2015.
 */
public class CTPreference {

    // Class variables
    private int colLength;
    private int brandLength;
    private int gearLength;
    private int fuelLength;


    //Preference Instances
    private int nrCompCars; // number of already compared cars
    private int sportScore; // sport Score of person
    private int familyScore; // family Score of person
    private int ecoScore; // eco Score of person
    private int priceScore; // price score of person
    private int offroadScore; // offroad score of person
    private int designScore; // design score of person

    private CTColor[] favColors; // MaxHeap with favorite ext. colors of user
    private CTBrand[] favBrands;// MaxHeap with fav. brands of user
    private CTGearType[] gearTypes; // MaxHeap with fav. gear type
    private CTFuelType[] fuelTypes; // MaxHeap with fav. fuel type

    public  CTPreference( String[] dataColors, String[] dataBrands, String[] dataGearTypes,
                          String[] dataFuelTypes){
        //Initialize array lengths
        fuelLength = dataFuelTypes.length;
        gearLength = dataGearTypes.length;
        brandLength = dataBrands.length;
        colLength = dataColors.length;

        //Initialize Instances
        nrCompCars = 0;
        sportScore = 0;
        familyScore = 0;
        ecoScore = 0;
        priceScore = 0;
        offroadScore = 0;
        designScore = 0;
        createFavColors(dataColors);
        createBrandTypes(dataBrands);
        createGearTypes(dataGearTypes);
        createFuelTypes(dataFuelTypes);
    }

    // Create Heaps
    public void createFavColors(String[] dataColors){
        favColors = new CTColor[colLength];
        for (int i = 0; i < colLength; ++i){
            favColors[i] = new CTColor(dataColors[i]);
        }
    }

    public void createBrandTypes(String[] dataBrands){
        favBrands = new CTBrand[brandLength];
        for(int i = 0; i < brandLength; ++i){
            favBrands[i] = new CTBrand(dataBrands[i]);
        }
    }

    public void createGearTypes(String[] dataGearTypes){
        gearTypes = new CTGearType[gearLength];
        for(int i = 0; i < gearLength; ++i){
            gearTypes[i] = new CTGearType(dataGearTypes[i]);
        }
    }

    public void createFuelTypes(String[] dataFuelTypes){
        fuelTypes = new CTFuelType[fuelLength];
        for(int i = 0; i < fuelLength; ++i){
            fuelTypes[i] = new CTFuelType(dataFuelTypes[i]);
        }
    }

    // Heap sorters

    public void colUpdater(String vColor){
        for (int i = 0; i < colLength; ++i) {
            if (vColor.equals(favColors[i].getColor())) {
                favColors[i].increaseColCount();
                CTColor temp;
                for (int j = 1; j < colLength; j++) {
                    for (int k = j - 1; (k >= 0) && (favColors[k].getColCount() < favColors[k + 1].getColCount()); k--) {
                        temp = favColors[k];
                        favColors[k] = favColors[k+1];
                        favColors[k+1] = temp;

                    }
                }

            }
        }
    }

    public void brandUpdater(String vBrand){
        for (int i = 0; i < brandLength; ++i)
            if (vBrand.equals(favBrands[i].getBrand())){
                favBrands[i].increaseBrandCount();
                CTBrand temp;
                for (int j = 1; j < brandLength; j++) {
                    for (int k = j - 1; (k >= 0) && (favBrands[k].getBrandCount() < favBrands[k + 1].getBrandCount()); k--) {
                        temp = favBrands[k];
                        favBrands[k] = favBrands[k+1];
                        favBrands[k+1] = temp;

                    }
                }
            }
    }

    public void gearUpdater(String vGear){
        for (int i = 0; i < gearLength; ++i)
            if (vGear.equals(gearTypes[i].getGearType())){
                gearTypes[i].increaseGearCount();
                CTGearType temp;
                for (int j = 1; j < gearLength; j++) {
                    for (int k = j - 1; (k >= 0) && (gearTypes[k].getGearCount() < gearTypes[k + 1].getGearCount()); k--) {
                        temp = gearTypes[k];
                        gearTypes[k] = gearTypes[k+1];
                        gearTypes[k+1] = temp;

                    }
                }
            }
    }

    public void fuelUpdater(String vFuel) {
        for (int i = 0; i < fuelLength; ++i)
            if (vFuel.equals(fuelTypes[i].getFuelType())){
                fuelTypes[i].increaseFuelCount();
                CTFuelType temp;
                for (int j = 1; j < fuelLength; j++) {
                    for (int k = j - 1; (k >= 0) && (fuelTypes[k].getFuelCount() < fuelTypes[k + 1].getFuelCount()); k--) {
                        temp = fuelTypes[k];
                        fuelTypes[k] = fuelTypes[k+1];
                        fuelTypes[k+1] = temp;

                    }
                }
            }
    }

    // Calculations

    public void computeSport(int vSport){
        sportScore = (sportScore*nrCompCars + vSport)/(nrCompCars+1);
    }

    public void computeFam(int vFam){
        familyScore = (familyScore*nrCompCars + vFam)/(nrCompCars+1);
    }

    public void computeEco(int vEco){
        ecoScore = (ecoScore*nrCompCars+ vEco)/(nrCompCars+1);
    }

    public void computePrice(int vPrice){
        priceScore = (priceScore*nrCompCars + vPrice)/(nrCompCars+1);
    }

    public void computeOffro(int vOffro){
        offroadScore = (offroadScore*nrCompCars + vOffro)/(nrCompCars+1);
    }

    public void computeDesign(int vDesign){
        designScore = (designScore*nrCompCars + vDesign)/(nrCompCars+1);
    }


    //Instances' setters and getters
    public int getNrCompCars() {
        return nrCompCars;
    }

    public void increaseNrCompCars() {
        this.nrCompCars += 1;
    }

    public int getSportScore() {
        return sportScore;
    }

    public void setSportScore(int sportScore) {
        this.sportScore = sportScore;
    }

    public int getFamilyScore() {
        return familyScore;
    }

    public void setFamilyScore(int familyScore) {
        this.familyScore = familyScore;
    }

    public int getEcoScore() {
        return ecoScore;
    }

    public void setEcoScore(int ecoScore) {
        this.ecoScore = ecoScore;
    }

    public int getPriceScore() {
        return priceScore;
    }

    public void setPriceScore(int priceScore) {
        this.priceScore = priceScore;
    }

    public int getOffroadScore() {
        return offroadScore;
    }

    public void setOffroadScore(int offroadScore) {
        this.offroadScore = offroadScore;
    }

    public int getDesignScore() {
        return designScore;
    }

    public void setDesignScore(int designScore) {
        this.designScore = designScore;
    }

    public CTColor[] getFavColors() {
        return favColors;
    }

    public void setFavColors(CTColor[] favColors) {
        this.favColors = favColors;
    }

    public CTBrand[] getFavBrands() {
        return favBrands;
    }

    public void setFavBrands(CTBrand[] favBrands) {
        this.favBrands = favBrands;
    }

    public CTGearType[] getGearTypes() {
        return gearTypes;
    }

    public void setGearTypes(CTGearType[] gearTypes) {
        this.gearTypes = gearTypes;
    }

    public CTFuelType[] getFuelTypes() {
        return fuelTypes;
    }

    public void setFuelTypes(CTFuelType[] fuelTypes) {
        this.fuelTypes = fuelTypes;
    }

}
