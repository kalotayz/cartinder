package ch.hackzurich15.cartinder.cartinder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Wudi on 03.10.2015.
 */
public class Vehicle {

    //Instances
    String vin;
    String brand;
    String modelGerman;
    String exteriorColor;
    String interiorColor;
    String gearType;
    String fuelType;
    int sportScore;
    int familyScore;
    int ecoScore;
    int priceScore;
    int offroadScore;
    int designScore;
    int vehicleImage;

    public Vehicle(String vin, String brand, String modelGerman, String extriorColor, String interiorColor,
                           String gearType, String fuelType, int sportScore, int familyScore,
                           int ecoScore, int priceScore, int offroadScore, int designScore, int vehicleImage) {

        this.vin = vin;
        this.brand = brand;
        this.modelGerman = modelGerman;
        this.exteriorColor = extriorColor;
        this.interiorColor = interiorColor;
        this.gearType = gearType;
        this.fuelType = fuelType;
        this.sportScore = sportScore;
        this.familyScore = familyScore;
        this.ecoScore = ecoScore;
        this.priceScore = priceScore;
        this.offroadScore = offroadScore;
        this.designScore = designScore;
        this.vehicleImage = vehicleImage;
    }


    //Getter methods
    public String getVin() {
        return vin;
    }

    public String getBrand() {
        return brand;
    }

    public String getModelGerman() {
        return modelGerman;
    }

    public String getExtriorColor() {
        return exteriorColor;
    }

    public String getInteriorColor() {
        return interiorColor;
    }

    public String getGearType() {
        return gearType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public int getSportScore() {
        return sportScore;
    }

    public int getFamilyScore() {
        return familyScore;
    }

    public int getEcoScore() {
        return ecoScore;
    }

    public int getPriceScore() {
        return priceScore;
    }

    public int getOffroadScore() {
        return offroadScore;
    }

    public int getVehicleImageId() {
        return vehicleImage;
    }

    public Bitmap getVehicleImage() throws IOException {

        URL url = new URL("http://api.hackzurich.amag.ch/hackzurich/image/"+ vehicleImage);
        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());

        return bmp;
    }

    public Bitmap getVehicleImage(int width) throws IOException {

        URL url = new URL("http://api.hackzurich.amag.ch/hackzurich/image/"+ vehicleImage +"/width" +width);
        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());

        return bmp;
    }

    public Bitmap getVehicleImageThumbnail() throws IOException {

        URL url = new URL("http://api.hackzurich.amag.ch/hackzurich/image/"+ vehicleImage +"/thumbnail");
        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());

        return bmp;
    }



    public int getDesignScore() {
        return designScore;
    }
}
