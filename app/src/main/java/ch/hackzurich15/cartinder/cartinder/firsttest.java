package ch.hackzurich15.cartinder.cartinder;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;

import ch.hackzurich15.cartinder.cartinder.database.DatabaseCommunicator;

public class firsttest extends AppCompatActivity {
    Vehicle mRandomVehicle1, mRandomVehicle2;
    Button button1, button2;
    TextView car1View, car2View;
    public static final String USER_PREFS_NAME ="userData";
    CTPerson mPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firsttest);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        car1View = (TextView) findViewById(R.id.carView1);
        car2View = (TextView) findViewById(R.id.carView2);


    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mRandomVehicle1 = DatabaseCommunicator.getRandomVehicle();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final SharedPreferences.Editor userDataEditor = getSharedPreferences(USER_PREFS_NAME,0).edit();
        car1View.setText(mRandomVehicle1.getBrand());
        car2View.setText(mRandomVehicle2.getBrand());
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPerson.startCalc(mRandomVehicle1);

            }
        });


    }
}
