package ch.hackzurich15.cartinder.cartinder;

/**
 * Created by Zsombor Kalotay on 03.10.2015.
 */
public class CTColor {

    private int colCount;
    private String color;

    public  CTColor(String color ){
        colCount = 0;
        this.color = color;

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getColCount() {
        return colCount;
    }

    public void increaseColCount() {
        colCount += 1;
    }


}
