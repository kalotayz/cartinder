package ch.hackzurich15.cartinder.cartinder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class MatcherActivity extends AppCompatActivity {
    ListView list;
    VehicleAdapter adapter;
    ArrayList<Vehicle> vehicleList;
    Button button1,button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matcher);

    }

    @Override
    protected void onStart() {
        super.onStart();

        list = (ListView) findViewById(R.id.listView);
        vehicleList = new ArrayList<Vehicle>();

        new VehicleAsyncTask().execute("http://api.hackzurich.amag.ch:8080/hackzurich/vehicle/random/2.json");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_matcher, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }






    public class VehicleAsyncTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            try {

                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(params[0]);
                HttpResponse response = client.execute(get);

                int status = response.getStatusLine().getStatusCode();
                if (status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    JSONObject jObj = new JSONObject(data);
                    JSONArray jArray =jObj.getJSONArray("vehicles");

                    for(int i=0; i<jArray.length(); i++){
                        JSONObject jRealObject = jArray.getJSONObject(i);

                        String vin = jRealObject.getString("vin");
                        String brand =jRealObject.getString("brand");
                        String modelGerman = jRealObject.getString("modelGerman");
                        String extriorColor = jRealObject.getString("exteriorColor");
                        String interiorColor = jRealObject.getString("interiorColor");
                        String gearType = jRealObject.getString("gearType");
                        String fuelType = jRealObject.getString("fuelType");
                        int sportScore = jRealObject.getInt("sportScore");
                        int familyScore = jRealObject.getInt("familyScore");
                        int ecoScore = jRealObject.getInt("ecoScore");
                        int priceScore = jRealObject.getInt("priceScore");
                        int offroadScore = jRealObject.getInt("offroadScore");
                        int designScore = jRealObject.getInt("designScore");
                        JSONArray jImageArray = jRealObject.getJSONArray("vehicleImages");

                        int vehicleImage = jImageArray.getJSONObject(1).getInt("id");

                        Vehicle vehicle = new Vehicle(vin, brand, modelGerman, extriorColor, interiorColor, gearType,
                                fuelType, sportScore, familyScore, ecoScore, priceScore, offroadScore,
                                designScore, vehicleImage);
                        vehicleList.add(vehicle);

                    }

                    return true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(result == false){
                // show a msg that data was not parsed
            } else{
                VehicleAdapter adapter = new VehicleAdapter(getApplicationContext(),R.layout.row,vehicleList);
                        list.setAdapter(adapter);
            }
        }
    }

}

