package ch.hackzurich15.cartinder.cartinder.database;
import android.test.mock.MockContext;

import org.json.*;

import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import ch.hackzurich15.cartinder.cartinder.Vehicle;

/**
 * Created by Wudi on 03.10.2015.
 */
public class DatabaseCommunicator {
    private static String API_URL = "http://api.hackzurich.amag.ch/hackzurich/";
    private static VehicleDatabaseManager dbManager = null;



    //Search within the database
    public static Vehicle[] getVehiclesByFeatures(String[] features) throws JSONException {
        if (dbManager == null) {
            dbManager = new VehicleDatabaseManager(new MockContext());
            dbManager.initialize(fetchData("http://api.hackzurich.amag.ch/hackzurich/dealer/100/vehicles.json","vehicles"));
        }
        Vehicle[] foundVehicles = dbManager.searchVehicles(features);
        return null;

    }

    public static Vehicle getRandomVehicle() throws JSONException {
        Random rnd = new Random(System.currentTimeMillis());
        String[] features = new String[11];
        for (int i = 0; i < features.length; i++) {
                switch (i) {
                    case 0:
                        features[i] = getArrayOfBrands()[rnd.nextInt(getNumberOfBrands())];
                        break;
                    case 1:
                        features[i] = getArrayOfExteriorColors()[rnd.nextInt(getNumberOfExteriorColors())];
                        break;
                    case 2:
                        features[i] = getArrayOfInteriorColors()[rnd.nextInt(getNumberOfInteriorColors())];
                        break;
                    case 3:
                        features[i] = getArrayOfGearTypes()[rnd.nextInt(getNumberOfGearTypes())];
                        break;
                    case 4:
                        features[i] = getArrayOfFuelTypes()[rnd.nextInt(getNumberOfFuelTypes())];
                        break;
                    case 5:
                        features[i] = Integer.toString(rnd.nextInt(5) + 1);
                        break;
                    case 6:
                        features[i] = Integer.toString(rnd.nextInt(5) + 1);
                        break;
                    case 7:
                        features[i] = Integer.toString(rnd.nextInt(5) + 1);
                        break;
                    case 8:
                        features[i] = Integer.toString(rnd.nextInt(5) + 1);
                        break;
                    case 9:
                        features[i] = Integer.toString(rnd.nextInt(5) + 1);
                        break;
                    case 10:
                        features[i] = Integer.toString(rnd.nextInt(5) + 1);
                        break;
                    default:
                        break;
                }

        }
        Vehicle[] potentialVehicals=  getVehiclesByFeatures(features);
        return  potentialVehicals[rnd.nextInt(potentialVehicals.length)];
    }

    //Get dynamically all features we need from the database
    public static int getNumberOfFuelTypes() {
        return 5;
    }

    public static String[] getArrayOfFuelTypes() {
        JSONArray exteriorColorArray = fetchData(API_URL + "vehicle/overview.json","vehicles");
        String[] output = new String[0];
        try {
            output = DifferentValues(exteriorColorArray, "fuelType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static int getNumberOfGearTypes() {
        return 85;
    }

    public static String[] getArrayOfGearTypes() {
        JSONArray exteriorColorArray = fetchData(API_URL + "geartype.json","vehicles");
        String[] output = new String[0];
        try {
            output = DifferentValues(exteriorColorArray, "gearType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static int getNumberOfBrands() {
        return 5;
    }

    public static String[] getArrayOfBrands() {
        JSONArray exteriorColorArray = fetchData(API_URL + "vehicle/overview.json","vehicles");
        String[] output = new String[0];
        try {
            output = DifferentValues(exteriorColorArray, "brands");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static int getNumberOfExteriorColors() {
        JSONArray exteriorColorArray = fetchData(API_URL + "color/exterior.json","colors");
        int output = -1;
        try {
            output = countDifferentValues(exteriorColorArray, "category");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static String[] getArrayOfExteriorColors() {
        JSONArray exteriorColorArray = fetchData(API_URL + "color/exterior.json","colors");
        String[] output = new String[0];
        try {
            output = DifferentValues(exteriorColorArray, "category");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static int getNumberOfInteriorColors() {
        JSONArray exteriorColorArray = fetchData(API_URL + "color/interior.json","colors");
        int output = -1;
        try {
            output = countDifferentValues(exteriorColorArray, "category");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }

    public static String[] getArrayOfInteriorColors() {
        JSONArray exteriorColorArray = fetchData(API_URL + "color/interior.json","colors");
        String[] output = new String[0];
        try {
            output =  DifferentValues(exteriorColorArray, "category");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return output;
    }


    //helper methods to avoid code duplication a little bit
    private static int countDifferentValues(JSONArray array, String value) throws JSONException {
        return DifferentValues(array, value).length;
    }

    private static String[] DifferentValues(JSONArray array, String value) throws JSONException {
        LinkedList<String> list = new LinkedList<String>();
        for (int i = 0; i < array.length(); i++) {
            if(!list.contains(array.getJSONObject(i).get(value).toString())) {
                list.add(array.getJSONObject(i).get(value).toString());
            }
        }
        String[] outputArray = new String[list.size()];
        return list.toArray(outputArray);
    }


    //helper methods to get JSON-data from the database
    private static JSONArray fetchData(String url, String type) {
        JSONObject json = null;
        JSONArray resultArray = new JSONArray();
        try {
            json = readJsonFromUrl(url);
            resultArray =  new JSONArray(json.get(type).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return resultArray;


    }

    private static JSONObject readJsonFromUrl(String url) throws ExecutionException, InterruptedException {
        MyAsyncTask myAsyncTask = new MyAsyncTask(url);
        return myAsyncTask.execute().get();
    }

    //Source: http://stackoverflow.com/questions/4308554/simplest-way-to-read-json-from-a-url-in-java
    /*private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    //Source: http://stackoverflow.com/questions/4308554/simplest-way-to-read-json-from-a-url-in-java
    private static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }*/
}
