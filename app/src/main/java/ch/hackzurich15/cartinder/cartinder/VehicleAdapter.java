package ch.hackzurich15.cartinder.cartinder;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 * Created by Tolga Aras on 03.10.2015.
 */
public class VehicleAdapter extends ArrayAdapter<Vehicle> {
    final SharedPreferences mPrefs = this.getContext().getSharedPreferences("userData", 0);
    final SharedPreferences.Editor userDataEditor = this.getContext().getSharedPreferences("userData", 0).edit();

    ArrayList<Vehicle> ArrayListVehicles;
    int Resource;
    Context context;
    LayoutInflater vi;

    public VehicleAdapter(Context context, int resource, ArrayList<Vehicle> objects) {
        super(context, resource, objects);
        ArrayListVehicles = objects;
        Resource = resource;
        this.context = context;

        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        ViewHolder holder;
        if(convertView == null) {
            convertView = vi.inflate(Resource, null);
            holder = new ViewHolder();

            holder.imageview = (ImageView) convertView.findViewById(R.id.carImageView);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.titleTextView);
            holder.info1TextView = (TextView) convertView.findViewById(R.id.info1TextView);
            holder.info2TextView = (TextView) convertView.findViewById(R.id.info2TextView);
            holder.info3TextView = (TextView) convertView.findViewById(R.id.info3TextView);
            holder.info4TextView = (TextView) convertView.findViewById(R.id.info4TextView);
            convertView.setTag(holder);
        }       else{
            holder = (ViewHolder)convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("watch!!   " + ArrayListVehicles.get(position).getBrand());
                Gson gson = new Gson();
                String json = mPrefs.getString("User", "");
                CTPerson mPerson = gson.fromJson(json, CTPerson.class);
                mPerson.startCalc(ArrayListVehicles.get(position));
                String jsonPerson = gson.toJson(mPerson);
                userDataEditor.putString("User", jsonPerson).commit();
                final Intent intent = new Intent(context, MatcherActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        holder.imageview.setImageResource(R.mipmap.ic_launcher);
        new DownloadImageTask(holder.imageview).execute("http://api.hackzurich.amag.ch/hackzurich/image/" + ArrayListVehicles.get(position).getVehicleImageId());
        holder.titleTextView.setText(ArrayListVehicles.get(position).getBrand());
        holder.info1TextView.setText(ArrayListVehicles.get(position).getModelGerman());
        holder.info2TextView.setText("SportScore " + ArrayListVehicles.get(position).getSportScore());
        holder.info3TextView.setText("FamilyScore " + ArrayListVehicles.get(position).getFamilyScore());
        holder.info4TextView.setText("EcoScore " + ArrayListVehicles.get(position).getEcoScore());
     /**   holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String json = mPrefs.getString("User", "");
                CTPerson mPerson = gson.fromJson(json, CTPerson.class);
                mPerson.startCalc(ArrayListVehicles.get(position));
                String jsonPerson = gson.toJson(mPerson);
                userDataEditor.putString("User", jsonPerson).commit();


            }
        }); **/


        return convertView;
    }

    static class ViewHolder {
        public ImageView imageview;
        public TextView titleTextView;
        public TextView info1TextView;
        public TextView info2TextView;
        public TextView info3TextView;
        public TextView info4TextView;


    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }

    }



}
