package ch.hackzurich15.cartinder.cartinder;

/**
 * Created by Zsombor Kalotay on 03.10.2015.
 */
public class CTFuelType {
    private int fuelCount;
    private String fuelType;

    public  CTFuelType(String fuelType ){
        fuelCount = 0;
        this.fuelType = fuelType;

    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public int getFuelCount() {
        return fuelCount;
    }

    public void increaseFuelCount() {
        fuelCount += 1;
    }


}
