package ch.hackzurich15.cartinder.cartinder;

/**
 * Created by Zsombor Kalotay on 03.10.2015.
 */
public class CTGearType {
    private int gearCount;
    private String gearType;

    public  CTGearType(String gearType ){
        gearCount = 0;
        this.gearType = gearType;

    }

    public String getGearType() {
        return gearType;
    }

    public void setGearTypeColor(String gearType) {
        this.gearType = gearType;
    }

    public int getGearCount() {
        return gearCount;
    }

    public void increaseGearCount() {
        gearCount += 1;
    }

}
