package ch.hackzurich15.cartinder.cartinder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;

import ch.hackzurich15.cartinder.cartinder.Vehicle;

/**
 * Created by Wudi on 03.10.2015.
 */
public class VehicleDatabaseManager extends SQLiteOpenHelper{




        // All Static variables
        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "Vehicles.db";

        // Contacts table name
        private static final String TABLE_VEHICLES = "vehicles";

        // Contacts Table Columns names
        private static final String KEY_ID = "id";
        private static final String KEY_VIN = "vin";
        private static final String KEY_BRAND = "brand";
        private static final String KEY_EX_COLOR = "exteriorColor";
        private static final String KEY_IN_COLOR = "interiorColor";
        private static final String KEY_GEARTYPE= "gearType";
        private static final String KEY_FUELTYPE = "fuelType";
        private static final String KEY_SPORTSCORE = "sportScore";
        private static final String KEY_FAMILYSCORE = "familyScore";
        private static final String KEY_ECOSCORE = "ecoScore";
        private static final String KEY_PRICESCORE = "priceScore";
        private static final String KEY_OFFROADSCORE = "offroadScore";
        private static final String KEY_DESIGNSCORE = "designScore";


        public VehicleDatabaseManager(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            String CREATE_VEHICLES_TABLE = "CREATE TABLE " + TABLE_VEHICLES + "("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_VIN + " TEXT,"
                    + KEY_BRAND+ " TEXT,"
                    + KEY_EX_COLOR + " TEXT,"
                    + KEY_IN_COLOR + " TEXT,"
                    + KEY_GEARTYPE + " TEXT,"
                    + KEY_FUELTYPE + " TEXT,"
                    + KEY_SPORTSCORE+ " INTEGER,"
                    + KEY_FAMILYSCORE + " INTEGER,"
                    + KEY_ECOSCORE + " INTEGER,"
                    + KEY_PRICESCORE + " INTEGER,"
                    + KEY_OFFROADSCORE+ " INTEGER,"
                    + KEY_DESIGNSCORE + " INTEGER" + ")";
            db.execSQL(CREATE_VEHICLES_TABLE);
        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_VEHICLES);

            // Create tables again
            onCreate(db);
        }

        public void addVehicle(String vin, String brand, String extriorColor, String interiorColor,
                              String gearType, String fuelType, int sportScore, int familyScore,
                              int ecoScore, int priceScore, int offroadScore, int designScore) {

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_VIN, vin);
            values.put(KEY_BRAND, brand);
            values.put(KEY_EX_COLOR, extriorColor);
            values.put(KEY_IN_COLOR, interiorColor);
            values.put(KEY_GEARTYPE, gearType);
            values.put(KEY_FUELTYPE, fuelType);
            values.put(KEY_SPORTSCORE, sportScore);
            values.put(KEY_FAMILYSCORE, familyScore);
            values.put(KEY_ECOSCORE, ecoScore);
            values.put(KEY_PRICESCORE, priceScore);
            values.put(KEY_OFFROADSCORE, offroadScore);
            values.put(KEY_DESIGNSCORE, designScore);


         }



        public void initialize(JSONArray jsonArray) throws JSONException {
            String vin;
            String brand;
            String extriorColor;
            String interiorColor;
            String gearType;
            String fuelType;
            int sportScore;
            int familyScore;
            int ecoScore;
            int priceScore;
            int offroadScore;
            int designScore;
            int vehicleImage;
            for(int i = 0; i < jsonArray.length(); i++) {
                vin = jsonArray.getJSONObject(i).getString("vin");
                brand = jsonArray.getJSONObject(i).getString("brand");
                extriorColor = jsonArray.getJSONObject(i).getString("extriorColor");
                interiorColor = jsonArray.getJSONObject(i).getString("interiorColor");
                gearType = jsonArray.getJSONObject(i).getString("gearType");
                fuelType = jsonArray.getJSONObject(i).getString("fuelType");
                familyScore = jsonArray.getJSONObject(i).getInt("familyScore");
                ecoScore = jsonArray.getJSONObject(i).getInt("ecoScore");
                priceScore = jsonArray.getJSONObject(i).getInt("priceScore");
                offroadScore = jsonArray.getJSONObject(i).getInt("offroadScore");
                designScore = jsonArray.getJSONObject(i).getInt("designScore");
                sportScore = jsonArray.getJSONObject(i).getInt("sportScore");
                vehicleImage = new JSONArray(jsonArray.getJSONObject(i).getString("vehicleImages")).getJSONObject(0).getInt("id");


                addVehicle(vin, brand, extriorColor, interiorColor,
                        gearType, fuelType, sportScore, familyScore,
                ecoScore, priceScore, offroadScore, designScore);
            }

        }

    public Vehicle[] searchVehicles(String[] features) {
        String sqlQuery = "SELECT * FROM " + TABLE_VEHICLES + " WHERE";
        boolean firstFeature = true;
        for(int i = 0; i < features.length; i++) {
            if (firstFeature) {
                firstFeature = false;
            } else {
                sqlQuery += " AND ";
            }
            switch(i) {
                case(0):  sqlQuery += sqlQuery + KEY_BRAND + "= \""+ features[0] +" \"";
                    break;
                case(1):  sqlQuery += sqlQuery + KEY_EX_COLOR + "= \""+ features[1] +" \"";
                    break;
                case(2):  sqlQuery += sqlQuery + KEY_IN_COLOR + "= \""+ features[2] +" \"";
                    break;
                case(3):  sqlQuery += sqlQuery + KEY_GEARTYPE + "= \""+ features[3] +" \"";
                    break;
                case(4):  sqlQuery += sqlQuery + KEY_FUELTYPE + "= \""+ features[4] +" \"";
                    break;
                case(5):  sqlQuery += sqlQuery + KEY_SPORTSCORE + "= " + features[5] ;
                    break;
                case(6):  sqlQuery += sqlQuery + KEY_FAMILYSCORE + "= " + features[6] ;
                    break;
                case(7):  sqlQuery += sqlQuery + KEY_ECOSCORE + "= " + features[7] ;
                    break;
                case(8):  sqlQuery += sqlQuery + KEY_PRICESCORE + "= " + features[8] ;
                    break;
                case(9):  sqlQuery += sqlQuery + KEY_OFFROADSCORE + "= " + features[9] ;
                    break;
                case(10):  sqlQuery += sqlQuery + KEY_DESIGNSCORE + "= " + features[10] ;
                    break;
            }
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        cursor.moveToFirst();
        Vehicle[] vehiclesArray = new Vehicle[cursor.getCount()];
        for (int i = 0; i < cursor.getCount(); i++) {
            vehiclesArray[i] = new Vehicle(cursor.getString(0), cursor.getString(1),"",
                    cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),
                    cursor.getInt(6), cursor.getInt(7),cursor.getInt(8),cursor.getInt(9),cursor.getInt(10),
                    cursor.getInt(11),cursor.getInt(12));
        }
        return vehiclesArray;
    }
}
