package ch.hackzurich15.cartinder.cartinder.database;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by Tolga Aras on 03.10.2015.
 */
public class MyAsyncTask extends AsyncTask<String,Void,JSONObject> {
    String url;

    public MyAsyncTask(String url){
        this.url = url;
    }

    @Override
    protected JSONObject doInBackground(String... params){

        InputStream is = null;
        try {
            is = new URL(url).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

            StringBuilder sb = new StringBuilder();
            int cp;
            try {
                while ((cp = rd.read()) != -1) {
                    sb.append((char) cp);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            String jsonText = sb.toString();
            JSONObject json = new JSONObject(jsonText);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
