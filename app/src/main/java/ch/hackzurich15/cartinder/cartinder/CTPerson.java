package ch.hackzurich15.cartinder.cartinder;

import android.content.SharedPreferences;
import android.location.Address;

import java.util.Date;

/**
 * Created by Zsombor Kalotay on 03.10.2015.
 */
public class CTPerson {
    private String username;
    private Date birthday;
    private String carType;
    private String eMail;
    private Address homeAddress, workAddress;
    private int numberOfChildren;
    private CTPreference preferences;

    public CTPerson(String username,String eMail, Date birthday, String carType,
                    Address homeAddress, Address workAddress, int numberOfChildren, String[] dataColors,
                    String[] dataBrands, String[] dataGearTypes, String[] dataFuelTypes) {


        this.username = username;
        this.eMail = eMail;
        this.birthday = birthday;
        this.carType = carType;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.numberOfChildren = numberOfChildren;
        this.preferences  = new CTPreference(dataColors,dataBrands, dataGearTypes, dataFuelTypes);
    }

    public void startCalc(Vehicle v){
        // Updates persons preferences

        preferences.computeSport(v.getSportScore());
        preferences.computeFam(v.getFamilyScore());
        preferences.computeEco(v.getEcoScore());
        preferences.computePrice(v.getPriceScore());
        preferences.computeOffro(v.getOffroadScore());
        preferences.computeDesign(v.getDesignScore());
        preferences.colUpdater(v.getExtriorColor());
        preferences.brandUpdater(v.getBrand());
        preferences.gearUpdater(v.getGearType());
        preferences.fuelUpdater(v.getFuelType());
        preferences.increaseNrCompCars();


    }

    //Getters and Setters

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public Date getBrithday() {
        return birthday;
    }

    public void setBrithday(Date brithday) {
        this.birthday = brithday;
    }


    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(Address workAddress) {
        this.workAddress = workAddress;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public CTPreference getPreferences() {
        return preferences;
    }

    public void setPreferences(CTPreference preferences) {
        this.preferences = preferences;
    }



}
